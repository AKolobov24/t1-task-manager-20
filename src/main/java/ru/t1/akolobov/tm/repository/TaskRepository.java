package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        List<Task> projectTasks = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId())) projectTasks.add(task);
        }
        return projectTasks;
    }

}
